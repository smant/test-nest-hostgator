import { IsNotEmpty, IsNumber, Length, MAX } from "class-validator";

export class CreateUserDto {

  @IsNotEmpty()
  firstName: string;

  @IsNotEmpty()
  lastName: string;

  @IsNotEmpty()
  isActive: boolean;

}