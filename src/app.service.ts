import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  
  getHello(): string {
    return 'Hello NestJS!';
  }

  getUsers(): string[]{
    return ['Ezra','Asaf','Aaron','Areli','Cesar']
  }  
}
